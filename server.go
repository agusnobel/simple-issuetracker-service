package main

import (
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"github.com/unrolled/render"
	"net/http"
	"strings"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"fmt"
	"time"
	"encoding/json"
)

type Issue struct {
	Id          int64
	Title       string
	Description string
	Status      string
}

type Comment struct {
	Id          int64
	Username    string
	Description string
	IssueId     int64
}

const port = ":3000"
const timeFormat = "2006-01-02 15:04:05"

var r *render.Render
var database *sql.DB

func main() {
	LoadLog("Initializing webservice...")

	LoadLog("Setting up sqlite database...")
	database, _ = sql.Open("sqlite3", "./storage.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS issue (id INTEGER PRIMARY KEY, title TEXT, description TEXT, status TEXT)")
	statement.Exec()
	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS comment (id INTEGER PRIMARY KEY, username TEXT, description TEXT, issue_id INTEGER, FOREIGN KEY (issue_id) REFERENCES issue(id))")
	statement.Exec()
	statement, _ = database.Prepare("CREATE TRIGGER IF NOT EXISTS delete_comments_before_delete_issue BEFORE DELETE ON issue BEGIN DELETE FROM comment WHERE issue_id = old.id; END;")
	statement.Exec()

	LoadLog("Setting up router...")
	r = render.New(render.Options{
		IndentJSON: true,
	})
	router := mux.NewRouter()

	/** Routes */
	router.HandleFunc("/", GetRoot).Methods(http.MethodGet)
	router.HandleFunc("/", OptionsRoot).Methods(http.MethodOptions)
	router.HandleFunc("/issues", GetIssues).Methods(http.MethodGet)
	router.HandleFunc("/issues", PostIssues).Methods(http.MethodPost)
	router.HandleFunc("/issues", OptionsIssues).Methods(http.MethodOptions)
	router.HandleFunc("/issues/{id}", GetIssue).Methods(http.MethodGet)
	router.HandleFunc("/issues/{id}", PatchIssue).Methods(http.MethodPatch)
	router.HandleFunc("/issues/{id}", DeleteIssue).Methods(http.MethodDelete)
	router.HandleFunc("/issues/{id}", OptionsIssue).Methods(http.MethodOptions)
	router.HandleFunc("/issues/{id}/comments", GetIssueComments).Methods(http.MethodGet)
	router.HandleFunc("/issues/{id}/comments", PostIssueComments).Methods(http.MethodPost)
	router.HandleFunc("/issues/{id}/comments", OptionsIssueComments).Methods(http.MethodOptions)
	router.HandleFunc("/issues/{issue_id}/comments/{comment_id}", GetIssueComment).Methods(http.MethodGet)
	router.HandleFunc("/issues/{issue_id}/comments/{comment_id}", DeleteIssueComment).Methods(http.MethodDelete)
	router.HandleFunc("/issues/{issue_id}/comments/{comment_id}", OptionsIssueComment).Methods(http.MethodOptions)

	n := negroni.Classic()
	n.UseHandler(router)

	LoadLog(fmt.Sprintf("Starting webservice on %s", port))
	http.ListenAndServe(port, n)
}

func Log(message string, tag string) {
	if tag == "" {
		tag = "info"
	}

	fmt.Println(fmt.Sprintf("[%s][%s] %s", time.Now().Format(timeFormat), strings.ToUpper(tag), message))
}

func LoadLog(message string) {
	Log(message, "loading")
}

func GetRoot(writer http.ResponseWriter, request *http.Request) {
	r.JSON(writer, http.StatusOK, map[string]string{"hello": "world"})
}

func OptionsRoot(writer http.ResponseWriter, request *http.Request) {
	options := []string{http.MethodGet, http.MethodOptions}
	writer.Header().Set("Allow", strings.Join(options, ","))
}

func GetIssues(writer http.ResponseWriter, request *http.Request) {
	rows, _ := database.Query("SELECT id, title, description, status FROM issue")

	issues := []Issue{}

	for rows.Next() {
		var issue Issue
		rows.Scan(&issue.Id, &issue.Title, &issue.Description, &issue.Status)
		issues = append(issues, issue)
	}

	r.JSON(writer, http.StatusOK, issues)
}

func PostIssues(writer http.ResponseWriter, request *http.Request) {
	if request.Header.Get("Content-Type") != "application/json" {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	decoder := json.NewDecoder(request.Body)
	var issue Issue
	err := decoder.Decode(&issue)
	defer request.Body.Close()

	if err != nil || issue.Id != 0 || issue.Title == "" {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	statement, _ := database.Prepare("INSERT INTO issue (title, description, status) VALUES (?, ?, ?)")
	result, _ := statement.Exec(issue.Title, issue.Description, issue.Status)

	id, _ := result.LastInsertId()
	issue.Id = id

	writer.Header().Set("Location", fmt.Sprintf("/issues/%d", issue.Id))
	writer.WriteHeader(http.StatusCreated)
}

func OptionsIssues(writer http.ResponseWriter, request *http.Request) {
	options := []string{http.MethodGet, http.MethodPost, http.MethodOptions}
	writer.Header().Set("Allow", strings.Join(options, ","))
}

func GetIssue(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	var issue Issue
	row := database.QueryRow("SELECT id, title, description, status FROM issue WHERE id = ?", vars["id"])
	err := row.Scan(&issue.Id, &issue.Title, &issue.Description, &issue.Status)

	if err != nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	r.JSON(writer, http.StatusOK, issue)
}

func PatchIssue(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	var curIssue Issue
	row := database.QueryRow("SELECT id, title, description, status FROM issue WHERE id = ?", vars["id"])
	err := row.Scan(&curIssue.Id, &curIssue.Title, &curIssue.Description, &curIssue.Status)

	if err != nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	if request.Header.Get("Content-Type") != "application/json" {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	decoder := json.NewDecoder(request.Body)
	var issue Issue
	err = decoder.Decode(&issue)
	defer request.Body.Close()

	if err != nil || issue.Id != 0 {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	var sqlFmt = "UPDATE issue SET %s WHERE id = ?"
	var sqlSet = ""
	var args []interface{}

	if issue.Title != "" && issue.Title != curIssue.Title {
		sqlSet += "title = ?"
		args = append(args, issue.Title)
	}

	if issue.Description != curIssue.Description {
		if sqlSet != "" {
			sqlSet += ", "
		}

		sqlSet += "description = ?"
		args = append(args, issue.Description)
	}

	if issue.Status != curIssue.Status {
		if sqlSet != "" {
			sqlSet += ", "
		}

		sqlSet += "status = ?"
		args = append(args, issue.Status)
	}

	args = append(args, vars["id"])

	statement, _ := database.Prepare(fmt.Sprintf(sqlFmt, sqlSet))
	statement.Exec(args...)
	writer.WriteHeader(http.StatusNoContent)
}

func DeleteIssue(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	statement, _ := database.Prepare("DELETE FROM issue WHERE id = ?")
	statement.Exec(vars["id"])

	writer.WriteHeader(http.StatusNoContent)
}

func OptionsIssue(writer http.ResponseWriter, request *http.Request) {
	options := []string{http.MethodGet, http.MethodPatch, http.MethodDelete, http.MethodOptions}
	writer.Header().Set("Allow", strings.Join(options, ","))
}

func GetIssueComments(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	var issueId int64
	row := database.QueryRow("SELECT id FROM issue WHERE id = ?", vars["id"])
	err := row.Scan(&issueId)

	if err != nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	rows, _ := database.Query("SELECT id, username, description, issue_id FROM comment WHERE issue_id = ?", issueId)

	comments := []Comment{}

	for rows.Next() {
		var comment Comment
		rows.Scan(&comment.Id, &comment.Username, &comment.Description, &comment.IssueId)
		comments = append(comments, comment)
	}

	r.JSON(writer, http.StatusOK, comments)
}

func PostIssueComments(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	var issueId int64
	row := database.QueryRow("SELECT id FROM issue WHERE id = ?", vars["id"])
	err := row.Scan(&issueId)

	if err != nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	if request.Header.Get("Content-Type") != "application/json" {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	decoder := json.NewDecoder(request.Body)
	var comment Comment
	err = decoder.Decode(&comment)
	defer request.Body.Close()

	if err != nil || comment.Id != 0 || comment.Username == "" || comment.Description == "" || comment.IssueId != 0 {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	comment.IssueId = issueId
	statement, _ := database.Prepare("INSERT INTO comment (username, description, issue_id) VALUES (?, ?, ?)")
	result, _ := statement.Exec(comment.Username, comment.Description, comment.IssueId)

	id, _ := result.LastInsertId()
	comment.Id = id

	writer.Header().Set("Location", fmt.Sprintf("/issues/%d/comments/%d", comment.IssueId, comment.Id))
	writer.WriteHeader(http.StatusCreated)
}

func OptionsIssueComments(writer http.ResponseWriter, request *http.Request) {
	options := []string{http.MethodGet, http.MethodPost, http.MethodOptions}
	writer.Header().Set("Allow", strings.Join(options, ","))
}

func GetIssueComment(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	var comment Comment
	row := database.QueryRow("SELECT id, username, description, issue_id FROM comment WHERE id = ? AND issue_id = ?", vars["comment_id"], vars["issue_id"])
	err := row.Scan(&comment.Id, &comment.Username, &comment.Description, &comment.IssueId)

	if err != nil {
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	r.JSON(writer, http.StatusOK, comment)
}

func DeleteIssueComment(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	statement, _ := database.Prepare("DELETE FROM comment WHERE id = ? AND issue_id = ?")
	statement.Exec(vars["comment_id"], vars["issue_id"])

	writer.WriteHeader(http.StatusNoContent)
}

func OptionsIssueComment(writer http.ResponseWriter, request *http.Request) {
	options := []string{http.MethodGet, http.MethodDelete, http.MethodOptions}
	writer.Header().Set("Allow", strings.Join(options, ","))
}