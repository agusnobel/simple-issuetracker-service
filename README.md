# Simple Issue Tracker Service

This Simple Issue Tacker Service is a RESTful API made to get myself more familiar with Go.

This project uses [Glide](https://github.com/Masterminds/glide) for package management. This is to ensure that all packages are running on the right version.

## Getting started
After installing [Git](https://git-scm.com/), [Go](https://golang.org/) and [Glide](https://github.com/Masterminds/glide), follow the steps below to get this project running.

Clone this repository

```
git clone https://gitlab.com/agusnobel/simple-issuetracker-service.git
```

Install dependencies with Glide

```
glide install
```

Run the server

```
go run server.go
```

You will now have the Simple Issue Tracker Service running on `localhost:3000`.

## RESTful webservice examples
All data from and to the webservice is in `JSON` format with the `Content-Type: application/json` header. See the `Allow` header on an `OPTIONS` request to see the allowed methods on one of the endpoints.

### `/issues`
#### `GET`
**Request**
```
GET /issues HTTP/1.1
```

**Response**

```
Content-Type: application/json
Status: 200 OK
```

``` json
[
    {
        "Id": 1,
        "Title": "First issue",
        "Description": "This is the description of the first issue",
        "Status": "open"
    },
    {
        "Id": 2,
        "Title": "Second issue",
        "Description": "This is the description of the second issue",
        "Status": "closed"
    }
]
```

#### `POST`
**Request**

```
POST /issues HTTP/1.1
Content-Type: application/json
```

``` json
{
	"Title": "New issue",
	"Description": "This is the description of the new issue",
	"Status": "open"
}
```

**Response**
```
Location: /issues/{created_issue_id}
Status: 201 Created
```

### `/issues/{issue_id}`
#### `GET`
**Request**
```
GET /issues/{issue_id} HTTP/1.1
```

**Response**

```
Content-Type: application/json
Status: 200 OK
```

``` json
{
    "Id": 1,
    "Title": "First issue",
    "Description": "This is the description of the first issue",
    "Status": "open"
}
```

#### `PATCH`
`Id` can't be modified.

**Request**
```
PATCH /issues/{issue_id} HTTP/1.1
```

```
{
	"Status": "closed"
}
```

**Response**

```
Status: 204 No Content
```

#### `DELETE`
**Request**
```
DELETE /issues/{issue_id} HTTP/1.1
```

**Response**

```
Status: 204 No Content
```

### `/issues/{issue_id}/comments`
#### `GET`
**Request**
```
GET /issues/{issue_id}/comments HTTP/1.1
```

**Response**

```
Content-Type: application/json
Status: 200 OK
```

``` json
[
    {
        "Id": 1,
        "Username": "Lorem",
        "Description": "This is the description of the first comment",
        "IssueId": 1
    },
    {
        "Id": 2,
        "Username": "Ipsum",
        "Description": "This is the description of the second comment",
        "IssueId": 1
    }
]
```

#### `POST`
**Request**

```
POST /issues/{issue_id}/comments HTTP/1.1
Content-Type: application/json
```

``` json
{
	"Username": "Lorem",
	"Description": "This is the description of the new comment",
}
```

**Response**
```
Location: /issues/{issue_id}/comments/{created_comment_id}
Status: 201 Created
```

### `/issues/{issue_id}/comments/{comment_id}`
#### `GET`
**Request**
```
GET /issues/{issue_id}/comments/{comment_id} HTTP/1.1
```

**Response**

```
Content-Type: application/json
Status: 200 OK
```

``` json
{
    "Id": 1,
    "Username": "Lorem",
    "Description": "This is the description of the first comment",
    "IssueId": 1
}
```

#### `DELETE`
**Request**
```
DELETE /issues/{issue_id}/comments/{comment_id} HTTP/1.1
```

**Response**

```
Status: 204 No Content
```
